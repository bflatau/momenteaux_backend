const bcryptController = require('./bcryptController.js');
const User = require('../models').User;

module.exports = {
  createUser(req, res){
    
    const hashed = bcryptController.getHashedPassword(req.body.password);

    return User.create({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      passwordSalt: hashed.salt,
      passwordHash: hashed.passwordHash,
    })
      .then(user => res.status(201).send(user))
      .catch(error => res.status(500).send(error));
  }
}

