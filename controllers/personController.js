const tokenController = require('./tokenController.js');
const Person = require('../models').Person;
const { Op } = require("sequelize");




// async function checkForUnique(req, res) {
//   const matches = await Person.findAll({
//     where: {
//       [Op.and]: [
//         { firstName: req.body.firstName },
//         { lastName: req.body.lastName }
//       ]
//     }
//   });

//   if (matches[0] === undefined){
//     return false
//   } 

//   else{ return true}

// }


//HELPER FUNCTIONS
const createPersonDB = (req, res) => {
  // DO CODE TO CHECK IF USER ALREADY EXISTS


  return Person.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  })
    .then(person => res.status(201).send(person))
    .catch(error => res.status(500).send(error));
};





//API CALLS
async function createPerson(req, res){

  const matches = await Person.findAll({
    where: {
      [Op.and]: [
        { firstName: req.body.firstName },
        { lastName: req.body.lastName }
      ]
    }
  });

  if (matches[0] != undefined){
    res.json(
      
      {errors: [{message: 'ERROR: name is not unique'}]}
    );
  }

  else {
    const tokenResponse = tokenController.validateToken(req.body.token, tokenController.getSecret());
    (tokenResponse.sub) ? createPersonDB(req, res) : res.json('token verification error');
  }
     
};


exports.createPerson = createPerson;





