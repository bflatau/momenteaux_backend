const sequelize = require('../db');
const User = require('../models').User;
const bcryptController = require('./bcryptController.js');
const tokenController = require('./tokenController.js');



const assignToken = (dbuser, res) => {
    const payload = {
       sub: dbuser.userID,
       iss: 'auth-service',
    };
    const secret = tokenController.getSecret();
    const token = tokenController.getToken(payload, secret);
    res.json({token: token});
};

exports.loginUser = (req, res) => {
  User.findOne({
    where: {
      email: req.body.email,
    },
  }).then((dbuser) => {

    //CHECK IF USER (EMAIL ADDRESS) EXISTS
    if (dbuser) {
      const pass = req.body.password;
      const salt = dbuser.passwordSalt;
      const hash = dbuser.passwordHash;
      const isValid = bcryptController.checkPassword(pass, salt, hash);

      if (!isValid) {
        res.status(401);
        res.json({ err: 'Password invalid' });
      } else {
        assignToken(dbuser, res);
      }
    }

    //IF USER DOESN'T EXIST, RETURN WITH 401
    else{
      res.status(418); //this should be 401 when I'm done playing around
      res.json({ err: 'No user registered with this email' });
    }
    
  }).catch((err) => {
      console.log(err);
      res.status(500);
      res.json({err: err});
  });
};

// exports.logins_get = (req, res) => {
//   permissionController.hasPermission(req, res, 'get_logins', () => {
//     Logins.findAll().then((permissions) => {
//       res.status(200);
//       res.json(permissions);
//     })
//     .catch((err) => {
//       res.status(500);
//       res.send(err);
//     });
//   });
// };














// const { db } = require('../../db');
// const sql = require('../../sql').users;
// const bcryptController = require('../bcryptController');
// const { assignToken } = require('../tokenController');

// ///TODO: Ben make sure this is following best practices///
// exports.verify_password_and_user = (req, res) => {
//     db.one(sql.getAdmin)
//     .then(dbData => {
//         const pass = req.body.password;
//         const bodyUser = req.body.username;
//         const dbUser = dbData.user_name;
//         const salt = dbData.password_salt;
//         const hash = dbData.password_hash;
//         const isValid = bcryptController.checkPassword(pass, salt, hash);
        
//         if (!isValid) {
//             res.status(400);
//             res.json({err: 'Password Invalid'});
//           } else if(bodyUser === dbUser) {
//             assignToken(dbData, res);
//             // res.json('match!');
//           } else {
//               res.status(400);
//               res.json({err: 'Username Invalid'})
//           }
//     })
//     .catch(error => {
//         console.log('ERROR:', error); // print error;
//         res.status(500);
//         res.json({err: err});
//     });
//   };