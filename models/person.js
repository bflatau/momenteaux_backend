'use strict';

// const { Sequelize, DataTypes} = require('sequelize');
// const { sequelize } = require('../db');

module.exports = (sequelize, DataTypes) => {
  const Person = sequelize.define('Person', {
    personID: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    firstName: DataTypes.STRING(50),
    lastName: DataTypes.STRING(100),
  },{tableName: 'people'});
  Person.associate = function (models) {
    // associations can be defined here

  };

  return Person;
};

