'use strict';

// const { Sequelize, DataTypes} = require('sequelize');
// const { sequelize } = require('../db');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    userID: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    firstName: DataTypes.STRING(50),
    lastName: DataTypes.STRING(100),
    passwordSalt: DataTypes.STRING,
    passwordHash: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: true
    }
  },{tableName: 'users'});
  User.associate = function (models) {
    // associations can be defined here

  };

  return User;
};

